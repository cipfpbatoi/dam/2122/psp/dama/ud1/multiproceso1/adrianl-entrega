package Ejercicio_2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

public class InOut {

    /**
     * Lee un string introducido por teclado
     *
     * @return
     */
    public String inString() {
        String entrada = null;
        try {
            entrada = b.readLine();
        } catch (Exception e) {
            this.salir();
        }
        return entrada;
    }

    /**
     * 
     * @param nombre
     * @return 
     */    
    public String inString(String nombre) {
        this.out(nombre);
        return this.inString();
    }
    
    /**
     * Lee un string introducido por teclado chequeando que cumple
     * con un patrón
     *
     * @param nombre
     * @param patron
     * @return
     */
    public String inString(String nombre, String patron) {
        InOut gestorIO = new InOut();
        String valor;
        boolean error;
        do {
            gestorIO.out(nombre);
            valor = gestorIO.inString();
            error = !Pattern.matches(patron, valor);
            if (error) {
                gestorIO.out("Error!!! El valor " + valor + " no cumple con el patrón");
            }
        } while (error);
        return valor;
    }

    /**
     * Lee un entero introducido por teclado
     *
     * @return
     */
    public int inInt() {
        int entrada = 0;
        try {
            entrada = Integer.parseInt(this.inString());
        } catch (Exception e) {
            this.salir();
        }
        return entrada;
    }
    
    /**
     * Lee un entero introducido por teclado entre un rango
     * de valores y con un nombre determinado
     *
     * @return
     */
    public int inInt(String nombre, int min, int max) {
        
        InOut gestorIO = new InOut();
        boolean error;
        int valor;

        do {
            gestorIO.out("¿"+nombre+"? ["+min+"-"+max+"]:");
            valor = gestorIO.inInt();
            error = !new Intervalo(min, max).incluye(valor);
            if (error) {
                gestorIO.out("Error!!! Debe ser un valor " + nombre + "válido");
            }
        } while (error);
        
        return valor;
    }

    /**
     * Lee un real de baja precisión introducido por teclado
     *
     * @return
     */
    public float inFloat() {
        float entrada = 0;
        try {
            entrada = Float.parseFloat(this.inString());
        } catch (Exception e) {
            this.salir();
        }
        return entrada;
    }

    /**
     * Lee un real de alta precisión introducido por teclado
     *
     * @return
     */
    public double inDouble() {
        double entrada = 0.0;
        try {
            entrada = Double.parseDouble(this.inString());
        } catch (Exception e) {
            this.salir();
        }
        return entrada;
    }
    
    /**
     * Lee un real de alta precisión introducido por teclado entre un rango
     * de valores y con un nombre determinado
     *
     * @return
     */
   public double inDouble(String nombre, int min, int max) {
        
        boolean error;
        double valor;

        do {
            this.out("¿"+nombre+"? ["+min+"-"+max+"]:");
            valor = this.inDouble();
            error = !new Intervalo(min, max).incluye(valor);
            if (error) {
                this.out("Error!!! Debe ser un valor " + nombre + "válido");
            }
        } while (error);
        
        return valor;
    }

    /**
     * Lee un entero largo introducido por teclado
     *
     * @return
     */
    public long inLong() {
        long entrada = 0;
        try {
            entrada = Long.parseLong(this.inString());
        } catch (Exception e) {
            this.salir();
        }
        return entrada;
    }

    /**
     * Lee un entero byte introducido por teclado
     *
     * @return
     */
    public byte inByte() {
        byte entrada = 0;
        try {
            entrada = Byte.parseByte(this.inString());
        } catch (Exception e) {
            this.salir();
        }
        return entrada;
    }

    /**
     * Lee un entero corto introducido por teclado
     *
     * @return
     */
    public short inShort() {
        short entrada = 0;
        try {
            entrada = Short.parseShort(this.inString());
        } catch (Exception e) {
            this.salir();
        }
        return entrada;
    }

    /**
     * Lee un carácter introducido por teclado
     *
     * @return
     */
    public char inChar() {
        char caracter = ' ';
        String entrada = this.inString();
        if (entrada.length() > 1) {
            this.salir();
        } else {
            caracter = entrada.charAt(0);
        }
        return caracter;
    }

    /**
     * Lee un booleano introducido por teclado
     *
     * @return
     */
    public boolean inBoolean() {
        boolean entradaLogica = true;
        String entrada = this.inString();
        if (entrada.equalsIgnoreCase("true") || entrada.equalsIgnoreCase("false")) {
            entradaLogica = Boolean.valueOf(entrada).booleanValue();
        } else {
            this.salir();
        }
        return entradaLogica;
    }
    
    /**
     * Imprime un string
     *
     * @param salida
     */
    public void out(String salida) {
        System.out.print(salida);
    }

    /**
     * Imprime un entero
     *
     * @param salida
     */
    public void out(int salida) {
        System.out.print(salida);
    }

    /**
     * Imprime un real de baja precisión
     *
     * @param salida
     */
    public void out(float salida) {
        System.out.print(salida);
    }

    /**
     * Imprime un real de alta precisión
     *
     * @param salida
     */
    public void out(double salida) {
        System.out.print(salida);
    }

    /**
     * Imprime un entero largo
     *
     * @param salida
     */
    public void out(long salida) {
        System.out.print(salida);
    }

    /**
     * Imprime un entero byte
     *
     * @param salida
     */
    public void out(byte salida) {
        System.out.print(salida);
    }

    /**
     * Imprime un entero corto
     *
     * @param salida
     */
    public void out(short salida) {
        System.out.print(salida);
    }

    /**
     * Imprime un caracter
     *
     * @param salida
     */
    public void out(char salida) {
        System.out.print(salida);
    }

    /**
     * Imprime un booleano
     *
     * @param salida
     */
    public void out(boolean salida) {
        System.out.print(salida);
    }
    
    /**
     * Imprime un objeto
     * @param objeto 
     */
    
    public void out(Object objeto) {
        System.out.print(objeto);
    }

    private void salir() {
        System.out.println("ERROR de entrada/salida");
        System.exit(0);
    }

    private static BufferedReader b = new BufferedReader(new InputStreamReader(System.in));

  
}
