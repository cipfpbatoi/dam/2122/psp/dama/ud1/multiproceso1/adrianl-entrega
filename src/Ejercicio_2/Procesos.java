package Ejercicio_2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Procesos {
    public static void main(String[] args) {
        String comando = "java -jar C:\\Users\\adria\\Desktop\\psppracticas\\Ejercicio2\\out\\artifacts\\Ejercicio2_jar\\Ejercicio2.jar";
        List<String> lista = new ArrayList<>(Arrays.asList(comando.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(lista);
        pb.inheritIO();
        //pb.redirectError(ProcessBuilder.Redirect.INHERIT);
        pb.redirectOutput(ProcessBuilder.Redirect.to(new File("C:\\Users\\adria\\Desktop\\entregas-psp\\randoms.txt")));


        try {
            Process procees = pb.start();
            procees.waitFor(2, TimeUnit.SECONDS);

        } catch (IOException | InterruptedException ex) {
            System.out.println(ex.getMessage());
        }


    }
}
