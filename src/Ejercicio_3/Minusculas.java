package Ejercicio_3;

import java.util.Scanner;

public class Minusculas {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        String line = teclado.nextLine();

        while (!(line.equals("finalizar"))) {
            String minus = line.toLowerCase();
            System.out.println(minus);
            line = teclado.nextLine();
        }
    }
}
